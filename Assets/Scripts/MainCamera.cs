﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

    public bool fadeToBlack;

	// Update is called once per frame
	void Update () {
        if (fadeToBlack) GetComponent<Camera>().backgroundColor -= new Color(0.005f, 0.005f, 0.005f);
    }

    public void backgroundFlash(int receiveColor)
    {
        //0 = random, 1 = red, 2 = green, 3 = blue, 4 = collectible (orange/yellow)
        if (receiveColor == 0) GetComponent<Camera>().backgroundColor = new Color(Random.Range(0.02f, 0.25f), Random.Range(0.02f, 0.25f), Random.Range(0.02f, 0.25f));
        else if (receiveColor == 1) GetComponent<Camera>().backgroundColor = new Color(Random.Range(0.15f, 0.2f), 0.05f, 0.05f);
        else if (receiveColor == 2) GetComponent<Camera>().backgroundColor = new Color(0.05f, Random.Range(0.15f, 0.2f), 0.05f);
        else if (receiveColor == 3) GetComponent<Camera>().backgroundColor = new Color(0.05f, 0.05f, Random.Range(0.15f, 0.2f));
        else if (receiveColor == 4) GetComponent<Camera>().backgroundColor = new Color(0.35f / Random.Range(1.5f, 2.5f),  0.15f / Random.Range(1.5f, 2.5f), 0);
        fadeToBlack = true;
        stopFadeToBlack();
    }

    private IEnumerator stopFadeToBlack()
    {
        yield return new WaitForSeconds(2);
        fadeToBlack = false;
    }
}
