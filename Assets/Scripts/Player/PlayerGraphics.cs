﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGraphics : MonoBehaviour {

    public bool Spin;
    public float Timer;
    public GameObject PlayerRef;
    public GameObject CameraRef;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
        if (Timer > 0) Timer -= Time.deltaTime;
        else if (Timer <= 0) GetComponentInChildren<Animator>().SetBool("Reloc", false);

        if(FindObjectOfType<Player>()) transform.position = Vector3.MoveTowards(transform.position, PlayerRef.transform.position, (Vector3.Distance(transform.position, PlayerRef.transform.position) * 3.5f) * Time.deltaTime);
	}
}