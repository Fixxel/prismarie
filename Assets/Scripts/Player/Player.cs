﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    Animator PrismarieAnimator;
    Animator RGBAnimator;

    public bool CanMoveForward;
    public bool CanMoveBack;
    public bool CanMoveLeft;
    public bool CanMoveRight;

    public bool Moving;

    public GameObject PlayerGraphicsRef;

    public int CurrentColor;

    Vector2 touchStartPos;
    
    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        CurrentColor = 1; //Red - RGB
        PrismarieAnimator = PlayerGraphicsRef.GetComponentInChildren<Animator>();
        RGBAnimator = PlayerGraphicsRef.transform.GetChild(1).GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update()
    {

        //Movement - PC
        if (Input.GetKeyDown(KeyCode.W) && !Moving)         //FORWARD
        {
            MovePlayer(true, false, false, false);
        }
        else if (Input.GetKeyDown(KeyCode.S) && !Moving)    //BACK
        {
            MovePlayer(false, true, false, false);
        }
        if (Input.GetKeyDown(KeyCode.D) && !Moving)         //RIGHT
        {
            MovePlayer(false, false, true, false);
        }
        else if (Input.GetKeyDown(KeyCode.A) && !Moving)    //LEFT
        {
            MovePlayer(false, false, false, true);
        }
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene("MainMenu");
            gameObject.SetActive(false);
            PlayerGraphicsRef.SetActive(false);
        }

        //Movement - MOBILE
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchStartPos = Input.GetTouch(0).position;
            }   

            if (Input.GetTouch(0).phase == TouchPhase.Moved && !Moving)
            {
                if (Input.GetTouch(0).position.y > touchStartPos.y + 40 && Input.GetTouch(0).position.x < touchStartPos.x - 40)        //FORWARD
                {
                    MovePlayer(true, false, false, false);
                }
                else if (Input.GetTouch(0).position.y < touchStartPos.y - 40 && Input.GetTouch(0).position.x > touchStartPos.x + 40)   //BACK
                {
                    MovePlayer(false, true, false, false);
                }
                if (Input.GetTouch(0).position.x > touchStartPos.x + 40 && Input.GetTouch(0).position.y > touchStartPos.y + 40)        //RIGHT
                {
                    MovePlayer(false, false, true, false);
                }
                else if (Input.GetTouch(0).position.x < touchStartPos.x - 40 && Input.GetTouch(0).position.y < touchStartPos.y - 40)   //LEFT
                {
                    MovePlayer(false, false, false, true);
                }
            }
            //for resetting level
            if (touchStartPos.y > Screen.height - Screen.height / 10 && touchStartPos.x > Screen.width - Screen.width / 10)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    Reloc(new Vector3(0, 20, 0), true);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }

            if (Input.touchCount > 2)
            {
                SceneManager.LoadSceneAsync("MainMenu");
                gameObject.SetActive(false);
                PlayerGraphicsRef.SetActive(false);
            }

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<SegmentSurface>()) GetComponent<Rigidbody>().useGravity = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<SegmentSurface>()) transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
        //Getting available movement directions, first checking if components exist
        if (other.GetComponent<SegmentSurface>()) CanMoveForward = other.GetComponent<SegmentSurface>().CanMoveForward;
        if (other.GetComponent<SegmentSurface>()) CanMoveBack = other.GetComponent<SegmentSurface>().CanMoveBack;
        if (other.GetComponent<SegmentSurface>()) CanMoveLeft = other.GetComponent<SegmentSurface>().CanMoveLeft;
        if (other.GetComponent<SegmentSurface>()) CanMoveRight = other.GetComponent<SegmentSurface>().CanMoveRight;
    }

    private void OnTriggerExit(Collider other)
    {
        GetComponent<Rigidbody>().useGravity = true;
    }
    
    private void RotateColor()
    {
        CurrentColor++;
        if (CurrentColor > 3) CurrentColor = 1;
        RGBAnimator.SetInteger("Color", CurrentColor);
    }

    private void MovePlayer(bool inputIsForward, bool inputIsBack, bool inputIsRight, bool inputIsLeft)
    {
        Moving = true;

        if (CanMoveForward && inputIsForward)
        {
            PlayerGraphicsRef.transform.GetChild(0).eulerAngles = new Vector3(0, 0, 0);
            transform.position += new Vector3(0, 1, 1);
        }
        else if (CanMoveBack && inputIsBack)
        {
            PlayerGraphicsRef.transform.GetChild(0).eulerAngles = new Vector3(0, 180, 0);
            transform.position += new Vector3(0, 1, -1);
        }
        else if (CanMoveRight && inputIsRight)
        {
            PlayerGraphicsRef.transform.GetChild(0).eulerAngles = new Vector3(0, 90, 0);
            transform.position += new Vector3(1, 1, 0);
        }
        else if (CanMoveLeft && inputIsLeft)
        {
            PlayerGraphicsRef.transform.GetChild(0).eulerAngles = new Vector3(0, 260, 0);
            transform.position += new Vector3(-1, 1, 0);
        }

        RotateColor();
        PrismarieAnimator.SetBool("Moving", true);
        PrismarieAnimator.SetInteger("LegCycle", (PrismarieAnimator.GetInteger("LegCycle") == 0 ? 1 : 0));
        StartCoroutine(FreezePlayerInputs(0.40f));
    }

    //so that movements cannot be spammed (could cause unwanted movement which may be bad at later levels. Also, twitchy/weird animation.)
    private IEnumerator FreezePlayerInputs(float forSeconds)
    {
        yield return new WaitForSeconds(0.25f);
        PrismarieAnimator.SetBool("Moving", false);

        yield return new WaitForSeconds(forSeconds);
        //System.Array.ForEach(FindObjectsOfType<SegmentSurface>(), surface => surface.StepTaken());                //for timed segment surfaces
        if(FindObjectOfType<SegmentSurface>()) foreach (SegmentSurface segSurf in FindObjectsOfType<SegmentSurface>()) segSurf.StepTaken();                //for timed segment surfaces
        if(FindObjectOfType<Segment>()) foreach (Segment seg in FindObjectsOfType<Segment>()) seg.MoveSegment();                                    //for moving segments
        if(FindObjectOfType<MoonAspect>()) foreach (MoonAspect ma in FindObjectsOfType<MoonAspect>()) ma.UpdateMoveToLoc();                            //MoonAspect enemy movement
        if(FindObjectOfType<Orb>()) foreach (Orb o in FindObjectsOfType<Orb>()) o.UpdateMoveToLoc();                              //OrbAspect enemy movement
        if(FindObjectOfType<Pursuer>()) foreach (Pursuer p in FindObjectsOfType<Pursuer>()) p.UpdateMoveToLoc();                                    //Pursuer enemy movement

        Moving = false;
    }

    public void Reloc(Vector3 Relocation, bool resetColors)
    {
        if (resetColors)
        {
            //color reset to Red
            CurrentColor = 1;
            RGBAnimator.SetInteger("Color", CurrentColor);
        }
        //Relocating player, used for jumps and nextlevels
        PlayerGraphicsRef.GetComponent<PlayerGraphics>().Timer = 1f;
        PrismarieAnimator.SetBool("Reloc", true);
        transform.position = Relocation;
        GetComponent<Rigidbody>().useGravity = true;
    }
    
}
