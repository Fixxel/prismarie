﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    bool collected;
    
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0.5f, 0 * Time.deltaTime);
        if (collected)
        {
            transform.Rotate(0, Random.Range(10f, 35f), 0 * Time.deltaTime);
            transform.Translate(0, Random.Range(0.005f, 0.15f), 0 * Time.deltaTime);
            transform.localScale -= transform.localScale / 50;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        //Following this segmentsurface, in case segment is moving type
        if (other.GetComponent<SegmentSurface>() && !collected)
        {
            transform.position = Vector3.MoveTowards(transform.position, other.transform.position, Vector3.Distance(transform.position, other.transform.position) * Time.deltaTime);
        }
        //Player collecting this collectible
        if (other.GetComponent<Player>())
        {
            collected = true;
            GetComponent<BoxCollider>().enabled = false;

            //reward player
            SaveLoad.IncrementCollectiblesGot();

            //destroy this
            Destroy(gameObject, 1f);
        }
    }

    private void OnDestroy()
    {
        //load effect and handle removal
        GameObject I = GameObject.Instantiate(Resources.Load("CollectibleEffect")) as GameObject;
        I.transform.position = transform.position;
        Destroy(I, 1f);

        //flash background color
        if(FindObjectOfType<MainCamera>()) FindObjectOfType<MainCamera>().backgroundFlash(4);
    }
}
