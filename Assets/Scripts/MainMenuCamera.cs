﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCamera : MonoBehaviour {

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        GetComponent<Camera>().backgroundColor = new Color(Random.Range(0, 0.25f), Random.Range(0, 0.25f), Random.Range(0, 0.25f));
        StartCoroutine(ReshuffleBackgroundCol());
    }
    void Update () {
        GetComponent<Camera>().backgroundColor -= new Color(0.001f, 0.001f, 0.001f);
    }

    private IEnumerator ReshuffleBackgroundCol()
    {
        yield return new WaitForSeconds(Random.Range(2f, 4f));
        GetComponent<Camera>().backgroundColor = new Color(Random.Range(0, 0.25f), Random.Range(0, 0.25f), Random.Range(0, 0.25f));
        StartCoroutine(ReshuffleBackgroundCol());
    }
}
