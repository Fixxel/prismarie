﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SaveLoad 
{
    //public static SaveData save = new SaveData();

    static int ContinueLevel;
    static int UnlockedLevels;
    static int Collectibles;
    static int Score;
    static List<int> Leaderboard;

    //
    //SAVING
    public static void UpdateContinue(int receiveLevel)
    {
        ContinueLevel = receiveLevel;
        PlayerPrefs.SetInt("ContinueLevel", ContinueLevel);
        PlayerPrefs.Save();

        //save.ContinueLevel = receiveLevel;
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Create(Application.persistentDataPath + "/SaveData.gd");
        //bf.Serialize(file, save);
        //file.Close();
    }

    public static void UpdateUnlockedLevels(int receiveLevel)
    {
        UnlockedLevels = receiveLevel;
        PlayerPrefs.SetInt("UnlockedLevels", UnlockedLevels);
        PlayerPrefs.Save();

        //save.UnlockedLevels = receiveLevel;
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Create(Application.persistentDataPath + "/SaveData.gd");
        //bf.Serialize(file, save);
        //file.Close();
    }

    public static void IncrementCollectiblesGot()
    {
        Collectibles++;
        PlayerPrefs.SetInt("Collectibles", Collectibles);
        PlayerPrefs.Save();

        //save.CollectiblesGot++;
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Create(Application.persistentDataPath + "/SaveData.gd");
        //bf.Serialize(file, save);
        //file.Close();
    }

    //
    //LOADING
    public static int GetContinueLevel()
    {
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Open(Application.persistentDataPath + ("/SaveData.gd"), FileMode.Open);
        //SaveData get = (SaveData)bf.Deserialize(file);
        //file.Close();
        //return get.ContinueLevel;

        return PlayerPrefs.GetInt("ContinueLevel");
    }

    public static int GetUnlockedLevels()
    {
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Open(Application.persistentDataPath + ("/SaveData.gd"), FileMode.Open);
        //SaveData get = (SaveData)bf.Deserialize(file);
        //file.Close();
        //return get.UnlockedLevels;

        return PlayerPrefs.GetInt("UnlockedLevels");
    }

    public static int GetCollectiblesGot()
    {
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Open(Application.persistentDataPath + ("/SaveData.gd"), FileMode.Open);
        //SaveData get = (SaveData)bf.Deserialize(file);
        //file.Close();
        //return get.CollectiblesGot;

        return PlayerPrefs.GetInt("Collectibles");
    }
}
