﻿
[System.Serializable]
public class SaveData
{
    public int ContinueLevel = 0;
    public int UnlockedLevels = 0;
    public int CollectiblesGot = 0;
}

