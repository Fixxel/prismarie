﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pursuer : MonoBehaviour {

    //Pursuer, enemy that pursues player once it sees player on x or z axis.

    //sight booleans (chases player)
    public bool forward;
    public bool back;
    public bool left;
    public bool right;

    public bool facingPlayer;

    Vector3 moveTo;
    Vector3 rotateTo;

	// Use this for initialization
	void Start () {
        moveTo = transform.position;
        rotateTo = transform.eulerAngles;
	}
    
    private void Update()
    {
        //detecting player
        RaycastHit Rhit;
        if (Physics.SphereCast(transform.position, 0.7f, Vector3.forward, out Rhit))
        {
            if (Rhit.transform.GetComponent<Player>()) forward = true;
        }
        else if (Physics.SphereCast(transform.position, 0.7f, Vector3.back, out Rhit))
        {
            if (Rhit.transform.GetComponent<Player>()) back = true;
        }
        else if (Physics.SphereCast(transform.position, 0.7f, Vector3.left, out Rhit))
        {
            if (Rhit.transform.GetComponent<Player>()) left = true;
        }
        else if (Physics.SphereCast(transform.position, 0.7f, Vector3.right, out Rhit))
        {
            if (Rhit.transform.GetComponent<Player>()) right = true;
        }
        
        transform.position = Vector3.MoveTowards(transform.position, moveTo, Vector3.Distance(transform.position, moveTo) * 2.5f * Time.deltaTime);
        transform.GetChild(0).eulerAngles = Vector3.LerpUnclamped(transform.GetChild(0).eulerAngles, rotateTo, 5f * Time.deltaTime);
    }

    public void UpdateMoveToLoc()
    {
        //which way has player been spotted at?
        if (forward)
        {
            RotateOrMove(359.9f, 0, 1);
        }
        else if (back)
        {
            RotateOrMove(180, 0, -1);
        }
        else if (left)
        {
            RotateOrMove(270, -1, 0);
        }
        else if (right)
        {
            RotateOrMove(90, 1, 0);
        }
    }

    private void RotateOrMove(float receiveYrot, float receiveXpos, float receiveZpos)
    {
        if (!facingPlayer)
        {
            GetComponentInChildren<Animator>().SetBool("Idle", false);
            GetComponentInChildren<Animator>().SetBool("Turn", true);
            rotateTo = new Vector3(0, receiveYrot, 0);
            facingPlayer = true;
        }
        else if (facingPlayer)
        {
            GetComponentInChildren<Animator>().SetBool("Idle", false);
            GetComponentInChildren<Animator>().SetBool("Move", true);
            moveTo = new Vector3(transform.position.x + receiveXpos, transform.position.y, transform.position.z + receiveZpos);
        }
        facingPlayer = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            //reset sight booleans
            forward = false;
            back = false;
            left = false;
            right = false;
            facingPlayer = false;
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.transform.GetComponent<Player>())
        {
            col.transform.GetComponent<Player>().Reloc(new Vector3(0, 10, 0), true);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
