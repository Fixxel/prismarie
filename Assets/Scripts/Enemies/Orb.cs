﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Orb : MonoBehaviour
{
    //Orb, Enemy that follows its "orbiter/moon", which rotates otherwise clockwise, but never from forward to back
    //or left to right to prevent too small patrol area.

    //for restricting movement within segments (cant float/fly, go outside map)
    enum dirE {forward, back, left, right}
    bool forward;
    bool back;
    bool left;
    bool right;
    bool dirLock;

    dirE dir;

    Vector3 moveTo;
    Vector3 rotateTo;

    // Use this for initialization
    void Start()
    {
        moveTo = transform.position;
        rotateTo = new Vector3(0, 180, 0);
        //UpdateMoveToLoc();
    }

    private void Update()
    {

        //moves towards moveto
        transform.position = Vector3.MoveTowards(transform.position, moveTo, Vector3.Distance(transform.position, moveTo) * 10f * Time.deltaTime);

        //randomly rotates orb
        transform.GetChild(0).Rotate(0, Random.Range(-1, 1), 0 * Time.deltaTime);


        //rotates orbs orbiter
        transform.GetChild(1).eulerAngles = Vector3.Lerp(transform.GetChild(1).eulerAngles, rotateTo, 10f * Time.deltaTime);
    }

    public void UpdateMoveToLoc()
    {
        if(!dirLock){

            if(forward && dir != dirE.back){
                rotateTo = new Vector3(0, 180, 0);
                dir = dirE.forward;
            }
            else if(right && dir != dirE.left){
                rotateTo = new Vector3(0, 270, 0);
                dir = dirE.right;
            }
            else if(back && dir != dirE.forward){
                rotateTo = new Vector3(0, 0.001f, 0);
                dir = dirE.back;
            }
            else if(left && dir != dirE.right){
                rotateTo = new Vector3(0, 90, 0);
                dir = dirE.left;
            }
            dirLock = true;
        }

        else if(dirLock){
            if(dir == dirE.forward && forward){
                moveTo = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
            }
            else if(dir == dirE.right && right){
                moveTo = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
            }
            else if (dir == dirE.back && back){
                moveTo = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
            }
            else if (dir == dirE.left && left){
                moveTo = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);
            }
            else dirLock = false;
        }
        
    }

    //for reversing direction, for example when there's another enemy (same type or not)
    //should prevent clipping
    public void ReverseDir(){

        if(dir == dirE.forward){
            rotateTo = new Vector3(0, 0.001f, 0);
            transform.eulerAngles = new Vector3(0, 30f, 0);
            dir = dirE.back;
        }
        else if(dir == dirE.back){
            rotateTo = new Vector3(0, 180, 0);
            transform.eulerAngles = new Vector3(0, -30f, 0);
            dir = dirE.forward;
        }
        else if(dir == dirE.left){
            rotateTo = new Vector3(0, 270, 0);
            transform.eulerAngles = new Vector3(0, 30f, 0);
            dir = dirE.right;
        }
        else if(dir == dirE.right){
            rotateTo = new Vector3(0, 90, 0);
            transform.eulerAngles = new Vector3(0, -30f, 0);
            dir = dirE.left;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>())
        {
            other.GetComponent<Player>().Reloc(new Vector3(0, 10, 0), true);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (other.GetComponent<SegmentSurface>())
        {
            forward = other.GetComponent<SegmentSurface>().CanMoveForward;
            back = other.GetComponent<SegmentSurface>().CanMoveBack;
            left = other.GetComponent<SegmentSurface>().CanMoveLeft;
            right = other.GetComponent<SegmentSurface>().CanMoveRight;
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<SegmentSurface>())
        {
            forward = other.GetComponent<SegmentSurface>().CanMoveForward;
            back = other.GetComponent<SegmentSurface>().CanMoveBack;
            left = other.GetComponent<SegmentSurface>().CanMoveLeft;
            right = other.GetComponent<SegmentSurface>().CanMoveRight;
        }
    }
}