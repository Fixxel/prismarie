﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbOrbiter : MonoBehaviour {

	public GameObject orbRef;

	void OnTriggerStay(Collider other)
	{
		if(other.GetComponent<OrbOrbiter>()){
			orbRef.GetComponent<Orb>().ReverseDir();
		}
		else if(other.GetComponent<Orb>()) orbRef.GetComponent<Orb>().ReverseDir();;
	}
	
}
