﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PyramidAspect : MonoBehaviour
{
    public enum PossibleDirections { forward, back, left, right, custom };

    int LocInMovementList;
    int LocInCustomLocList;

    public List<PossibleDirections> MovementList = new List<PossibleDirections>();
    public List<Vector3> CustomLocList = new List<Vector3>();

    Vector3 MoveTo;

    // Use this for initialization
    void Start()
    {
        MoveTo = transform.position;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, MoveTo, 3f * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<Rigidbody>().useGravity = false;
    }

    public void UpdateMoveToLoc()
    {
        if (MovementList[LocInMovementList] == PossibleDirections.forward) MoveTo += new Vector3(0, 0, 1);
        else if (MovementList[LocInMovementList] == PossibleDirections.back) MoveTo += new Vector3(0, 0, -1);
        else if (MovementList[LocInMovementList] == PossibleDirections.left) MoveTo += new Vector3(-1, 0, 0);
        else if (MovementList[LocInMovementList] == PossibleDirections.right) MoveTo += new Vector3(1, 0, 0);
        else if (MovementList[LocInMovementList] == PossibleDirections.custom)
        {
            MoveTo += CustomLocList[LocInCustomLocList];
            LocInCustomLocList = LocInCustomLocList >= CustomLocList.Count - 1 ? 0 : LocInCustomLocList + 1;
        }

        LocInMovementList = LocInMovementList >= MovementList.Count - 1 ? 0 : LocInMovementList + 1;
    }
}
