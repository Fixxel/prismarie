﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : MonoBehaviour {

    public bool Open;

    public int thisColor;

    public Mesh EyeballRed;
    public Mesh EyeballGreen;
    public Mesh EyeballBlue;

    public List<Mesh> EyelidAMeshes = new List<Mesh>();
    public List<Mesh> EyelidBMeshes = new List<Mesh>();

	// Use this for initialization
	void Start () {
        //for randomizing eyelids
        transform.GetChild(1).GetComponent<MeshFilter>().mesh = EyelidAMeshes[Random.Range(1, EyelidAMeshes.Count)];
        transform.GetChild(2).GetComponent<MeshFilter>().mesh = EyelidBMeshes[Random.Range(1, EyelidBMeshes.Count)];
    }
	
    public void ToOpen(int receivedColor)
    {
        foreach (AnimatorControllerParameter p in GetComponent<Animator>().parameters) GetComponent<Animator>().SetBool(p.name, false);
        
        if (receivedColor == 1)
        {
            transform.GetChild(0).GetComponent<MeshFilter>().mesh = EyeballRed;
            GetComponent<Animator>().SetBool("CtoR", true);
        }else if (receivedColor == 2)
        {
            transform.GetChild(0).GetComponent<MeshFilter>().mesh = EyeballGreen;
            GetComponent<Animator>().SetBool("CtoG", true);
        }else if (receivedColor == 3)
        {
            transform.GetChild(0).GetComponent<MeshFilter>().mesh = EyeballBlue;
            GetComponent<Animator>().SetBool("CtoB", true);
        }
        thisColor = receivedColor;
        Open = true;
    }

    public void ToClosed()
    {
        foreach (AnimatorControllerParameter p in GetComponent<Animator>().parameters) GetComponent<Animator>().SetBool(p.name, false);

        if (thisColor == 1) GetComponent<Animator>().SetBool("RtoC", true);
        else if (thisColor == 2) GetComponent<Animator>().SetBool("GtoC", true);
        else if (thisColor == 3) GetComponent<Animator>().SetBool("BtoC", true);
        Open = false;
    }
}
