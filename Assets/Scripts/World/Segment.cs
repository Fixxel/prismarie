﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public bool IsMoving;

    public enum MoveDirections { forward, back, left, right, up, down, stay, custom };  //for setting a cycle in Unity editor where block moves (each time player moves)

    int LocInMoveToLocList;
    int LocInCustomLocList;

    public List<Mesh> BlockMeshes = new List<Mesh>();
    public List<Mesh> MovingSegmentMeshes = new List<Mesh>();
    public List<MoveDirections> MoveToLocList = new List<MoveDirections>();  //for setting a cycle in Unity editor where block moves (each time player moves)
    public List<Vector3> CustomLocList = new List<Vector3>();   //Custom location if not forward, back, left or right (can also be used for rotation)

    Vector3 MoveTo;

    // Use this for initialization
    void Start ()
    {
        MoveTo = transform.position;
        GetComponent<MeshFilter>().mesh = BlockMeshes[Random.Range(1, BlockMeshes.Count)];
        //if(IsMoving) GetComponent<MeshFilter>().mesh = MovingSegmentMeshes[Random.Range(1, MovingSegmentMeshes.Count)];;
    }

    private void Update()
    {
        if (IsMoving) transform.position = Vector3.MoveTowards(transform.position, MoveTo, Vector3.Distance(transform.position, MoveTo) * 4.5f * Time.deltaTime);
    }

    public void MoveSegment()
    {
        if (IsMoving)
        {
            //did simpler mechanism for this before with no if-else construct (with 1 int and 1 vector3 list)
            //but I think this is better & faster especially when building levels through Unity editor
            if (MoveToLocList[LocInMoveToLocList] == MoveDirections.forward) MoveTo += new Vector3(0, 0, 1);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.back) MoveTo += new Vector3(0, 0, -1);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.left) MoveTo += new Vector3(-1, 0, 0);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.right) MoveTo += new Vector3(1, 0, 0);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.up) MoveTo += new Vector3(0, 1, 0);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.down) MoveTo += new Vector3(0, -1, 0);
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.stay) {}   //stays where it is
            else if (MoveToLocList[LocInMoveToLocList] == MoveDirections.custom)
            {
                MoveTo += CustomLocList[LocInCustomLocList];
                LocInCustomLocList = LocInCustomLocList >= CustomLocList.Count - 1 ? 0 : LocInCustomLocList + 1;
            }

            LocInMoveToLocList = LocInMoveToLocList >= MoveToLocList.Count - 1 ? 0 : LocInMoveToLocList + 1;    //to move forward and cycle in MoveToLocationList
        }
    }
}