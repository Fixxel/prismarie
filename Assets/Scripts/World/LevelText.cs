﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelText : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<Text>().text = "Level " + FindObjectOfType<Level>().ThisLevel;
    }
}