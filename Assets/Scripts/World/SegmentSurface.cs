﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentSurface : Segment {

    public bool DoesHaveEye;
    public bool Collectible;
    public bool Pylon;

    public bool CanMoveForward;
    public bool CanMoveBack;
    public bool CanMoveLeft;
    public bool CanMoveRight;

    public bool IsJumpBlock;
    public bool IsTimed;
    public bool IsConnected;

    public bool IsStarterSegment;

    public GameObject EyeRef;
    public GameObject PlayerRef;

    public int thisColor;
    public int StepsToClosingEye;
    int StepsTaken;

    public Vector3 JumpLoc;
    
    // Use this for initialization
    void Start ()
    {
        if (DoesHaveEye)
        {
            EyeRef.SetActive(true);
            if (IsTimed)    //nested in case accidentally true
            {
                gameObject.AddComponent<TextMesh>().color = Color.white;
                GetComponent<TextMesh>().anchor = TextAnchor.LowerCenter;
                GetComponent<TextMesh>().fontSize = 20;
                GetComponent<TextMesh>().font = Resources.Load("fonts/EligibleSans-Regular.ttf") as Font;
                GetComponent<TextMesh>().characterSize = 0.5f;
                GetComponent<TextMesh>().color = new Color(Random.Range(0.3f, 0.7f), Random.Range(0.3f, 0.7f), Random.Range(0.3f, 0.7f));
            }
        }
        if (Collectible)
        {
            GameObject I = GameObject.Instantiate(Resources.Load("Collectible")) as GameObject;
            I.transform.position = transform.position;
        }
        if (IsStarterSegment)
        {
            GameObject I = GameObject.Instantiate(Resources.Load("startingSegmentEffect")) as GameObject;
            I.transform.position = transform.position;
            I.transform.parent = gameObject.transform;
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (DoesHaveEye && other.GetComponent<Player>()) ColorThis(other.GetComponent<Player>().CurrentColor);
        if (IsStarterSegment && other.GetComponent<Player>()) ShowHidden(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (IsJumpBlock && other.GetComponent<Player>()) FindObjectOfType<Player>().Reloc(JumpLoc, false);
    }

    private void OnTriggerExit(Collider other)
    {
        if (IsStarterSegment && other.GetComponent<Player>())
        {
            ShowHidden(false);
            IsStarterSegment = false;
        }
    }

    //
    //shows hidden objects when player is on starting/spawning segment (for example timed blocks)
    private void ShowHidden(bool showOrNot)
    {
        if (showOrNot)
        {
            foreach (SegmentSurface segSurf in FindObjectsOfType<SegmentSurface>())
            {
                if (segSurf.IsTimed) segSurf.gameObject.GetComponent<TextMesh>().text = "" + segSurf.StepsToClosingEye;
            }
        }
        else if (!showOrNot)
        {
            foreach (SegmentSurface segSurf in FindObjectsOfType<SegmentSurface>())
            {
                if (segSurf.IsTimed) segSurf.gameObject.GetComponent<TextMesh>().text = "";
            }
        }
    }

    //for timed eyes (closes when certain amount of steps have been taken by player)
    public void StepTaken()
    {
        if (EyeRef.GetComponent<Eye>().Open && IsTimed)
        {
            StepsTaken++;
            GetComponent<TextMesh>().text = (StepsToClosingEye - StepsTaken + 1).ToString();

            if (thisColor == 1) GetComponent<TextMesh>().color = new Color(0.95f, 0.1f, 0.1f, 0.85f);
            else if (thisColor == 2) GetComponent<TextMesh>().color = new Color(0.1f, 0.95f, 0.1f, 0.85f);
            else if (thisColor == 3) GetComponent<TextMesh>().color = new Color(0.1f, 0.1f, 0.95f, 0.85f);

            if (StepsTaken > StepsToClosingEye)
            {
                EyeRef.GetComponent<Eye>().ToClosed();
                FindObjectOfType<Level>().EyeClosing(thisColor);
                StepsTaken = 0;
                GetComponent<TextMesh>().text = "";
            }
        }
    }

    private void Update()
    {
        CanMoveForward = transform.GetChild(0).GetComponent<DirDetection>().CanMove;
        CanMoveBack = transform.GetChild(1).GetComponent<DirDetection>().CanMove;
        CanMoveLeft = transform.GetChild(2).GetComponent<DirDetection>().CanMove;
        CanMoveRight = transform.GetChild(3).GetComponent<DirDetection>().CanMove;
    }

    public void ColorThis(int receivedColor)
    {
        //if this has eye, if it is closed
        if (DoesHaveEye && !EyeRef.GetComponent<Eye>().Open)
        {
            EyeRef.GetComponent<Eye>().ToOpen(receivedColor);
            FindObjectOfType<Level>().EyeOpening(receivedColor);
            FindObjectOfType<MainCamera>().backgroundFlash(receivedColor);

            if (IsTimed) GetComponent<TextMesh>().text = "";
        }
        //if this has eye, if it is open and not about to close from number of stepstaken by player
        else if (DoesHaveEye && EyeRef.GetComponent<Eye>().Open && StepsTaken <= StepsToClosingEye)
        {
            EyeRef.GetComponent<Eye>().ToClosed();
            FindObjectOfType<Level>().EyeClosing(receivedColor);

            if (IsTimed) GetComponent<TextMesh>().text = "";
            StepsTaken = 0;
        }
        thisColor = receivedColor;
    }
    
    public void InkThis()
    {
        Debug.Log("Ink this segment");
    }


}
