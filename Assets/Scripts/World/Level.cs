﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour {

    public bool Finished;
    public bool Finishing;
    
    public int ThisLevel;
    public int SpotsToColor;
    public int SpotsColored;
    int RedCount;
    int GreenCount;
    int BlueCount;
    int ComboPoints;

    // Use this for initialization
    void Start () {
        //executes slight flash in the cameras background, which fades back to black
        if (FindObjectOfType<MainCamera>()) FindObjectOfType<MainCamera>().backgroundFlash(0);

        if (ThisLevel >= SaveLoad.GetContinueLevel()) SaveLoad.UpdateContinue(ThisLevel);
        ////unlock this level for level select (but only if it is not already unlocked)
        if (ThisLevel > SaveLoad.GetUnlockedLevels()) SaveLoad.UpdateUnlockedLevels(ThisLevel);

        if (FindObjectOfType<Collectible>())
        {
            foreach (Collectible c in FindObjectsOfType<Collectible>())
            {

            }
        }
    }

    //handling affect on score upon eye closing
    public void EyeClosing(int receivedColor)
    {
        //remove score for one eye
        if (FindObjectOfType<Score>()) FindObjectOfType<Score>().ScoreAmount -= 10;

        //remove combo point depending on the received color (color of the eye)
        if (receivedColor == 1) RedCount--;
        else if (receivedColor == 2) GreenCount--;
        else if (receivedColor == 3) BlueCount--;

        //remove spotscolored
        SpotsColored--;
    }

    //handling affect on score upon eye opening
    public void EyeOpening(int receivedColor)
    {
        //add score for one eye
        if (FindObjectOfType<Score>()) FindObjectOfType<Score>().ScoreAmount += 10;

        //add combo point depending on the received color (color of the eye)
        if (receivedColor == 1) RedCount++;
        else if (receivedColor == 2) GreenCount++;
        else if (receivedColor == 3) BlueCount++;

        //add spotscolored to the level and then check if == spotstocolor
        SpotsColored++;
        if (SpotsColored >= SpotsToColor && !Finished)
        {
            Finished = true;

            StartCoroutine(LevelFinished(1));
        }
    }
    
    private IEnumerator LevelFinished(float waitFor)
    {

        yield return new WaitForSeconds(waitFor);
        
        //relocating player
        if (FindObjectOfType<Player>()) FindObjectOfType<Player>().Reloc(new Vector3(0, 15, 0), true);

        //counting combo points from same colors on level
        if (RedCount > 1) ComboPoints += RedCount;
        if (GreenCount > 1) ComboPoints += GreenCount;
        if (BlueCount > 1) ComboPoints += BlueCount;
        //applying counted combo points to the score
        if (ComboPoints > 0) FindObjectOfType<Score>().ScoreAmount += (ComboPoints * 20);

        //loading next level and destroying this after 2 seconds
        SceneManager.LoadScene("Level" + (ThisLevel + 1).ToString());
    }

}
