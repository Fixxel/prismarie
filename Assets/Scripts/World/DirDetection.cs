﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirDetection : MonoBehaviour {

    public bool CanMove;

    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<DirDetection>() || other.GetComponent<SegmentSurface>()) CanMove = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.GetComponent<DirDetection>() || other.GetComponent<SegmentSurface>()) CanMove = false;
    }
}