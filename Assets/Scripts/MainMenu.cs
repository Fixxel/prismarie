﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    
    public Canvas NewGameT;
    public Canvas ContinueT;
    public Canvas LevelSelectT;

    public GameObject PlayerRef;
    public GameObject PlayerGraphicsRef;

    public GameObject cameraRef;
    
    Vector2 touchStartPos;

    private void Start()
    {
        if (SaveLoad.GetContinueLevel() < 1) SaveLoad.UpdateContinue(1);
    }

    // Update is called once per frame
    void Update()
    {
        NewGameT.GetComponent<Text>().fontSize = Screen.width / 9;
        NewGameT.GetComponent<Text>().color = Color.gray;
        ContinueT.GetComponent<Text>().fontSize = Screen.width / 9;
        ContinueT.GetComponent<Text>().color = Color.gray;
        LevelSelectT.GetComponent<Text>().fontSize = Screen.width / 9;
        LevelSelectT.GetComponent<Text>().color = Color.gray;

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).position.y > Screen.width / 6 && Input.GetTouch(0).position.y < Screen.width / 3)
            {
                NewGameT.GetComponent<Text>().color = Color.white;

                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    ActivatePlayer(true);
                    SceneManager.LoadScene("Level1");
                }
            }

            if (Input.GetTouch(0).position.y > Screen.width / 9 && Input.GetTouch(0).position.y < Screen.width / 6)
            {
                ContinueT.GetComponent<Text>().color = Color.white;

                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    ActivatePlayer(true);
                    SceneManager.LoadScene("Level" + SaveLoad.GetContinueLevel());
                }
            }

            if (Input.GetTouch(0).position.y > 0 && Input.GetTouch(0).position.y < Screen.width / 9)
            {
                LevelSelectT.GetComponent<Text>().color = Color.white;

                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    SceneManager.LoadScene("LevelSelect");
                }
            }
        }

        else if (Input.GetKey(KeyCode.Space))
        {
            ActivatePlayer(true);
            SceneManager.LoadScene("Level1");
            //SceneManager.LoadScene("Level25");  //testing
        }   
        else if (Input.GetKey(KeyCode.C) && SaveLoad.GetContinueLevel() > 0)
        {
            ActivatePlayer(true);
            SceneManager.LoadScene("Level" + SaveLoad.GetUnlockedLevels());
        }
    }


    private void ActivatePlayer(bool receive)
    {
        PlayerRef.SetActive(receive);
        PlayerGraphicsRef.SetActive(receive);
        Destroy(cameraRef);
    }
}
